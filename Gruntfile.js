module.exports = function (grunt) {
    
        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),
            coffee: {
                compile: {
                    options: { bare: false },
                    files: {
                        'app/<%= pkg.name %>.js': ['app/coffeescript/<%= pkg.name %>.coffee']
                    }
                },
                router: {
                    options: { bare: false },
                    files: {
                        'app/rojobaco-router-0.1.0.js': ['app/coffeescript/rojobaco-router-0.1.0.coffee']
                    }
                }
            },
            concat: {
                coffee: {
                    src: [
                        'app/coffeescript/route/route.coffee',
                        'app/coffeescript/templates/template.coffee',

                        'app/coffeescript/register/*.coffee',
                        'app/coffeescript/home/*.coffee',
                        'app/coffeescript/login/*.coffee',
                        
                        'app/coffeescript/route/routeHandler.coffee',
                        'app/coffeescript/root/initRouter.coffee'
                    ],
                    dest: 'app/coffeescript/<%= pkg.name %>.coffee'
                },
                router: {
                    src: [
                        'app/coffeescript/route/route.coffee',                       
                        'app/coffeescript/route/routeHandler.coffee',
                    ],
                    dest: 'app/coffeescript/rojobaco-router-0.1.0.coffee'
                },
                js: {
                    options: {
                        banner: "/*\n" +
                            "<%= pkg.name %> v<%= pkg.version %>\n" +
                            "Copyright (c) <%= (new Date()).getFullYear() %> <%= pkg.author.name %> All rights reserved.\n" +
                            "*/\n\n'use strict';\n\Rojobaco = window.Rojobaco = {};\n$ = jQuery;\n\n"
                    },
                    src: [ 'app/rojobaco-router-0.1.0.js' ],
                    dest: 'app/rojobaco-router-0.1.0.js'
                }
                
            },
            uglify: {
                my_target: {
                    files: [
                        {
                            src: 'app/<%= pkg.name %>.js',
                            dest: 'app/<%= pkg.name %>.min.js'
                        }
                    ]
                }
            }
        });
    
        grunt.loadNpmTasks('grunt-contrib-coffee');
        grunt.loadNpmTasks('grunt-contrib-concat');
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.registerTask('compile', ['concat:coffee', 'coffee']);
        grunt.registerTask('router', ['concat:router', 'coffee:router', 'concat:js']);
    };