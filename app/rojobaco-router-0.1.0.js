/*
rojobaco-router v0.1.0
Copyright (c) 2017 Rojobaco (Pty) Ltd All rights reserved.
*/

'use strict';
Rojobaco = window.Rojobaco = {};
$ = jQuery;

(function() {
  Rojobaco.Util = {};

  Rojobaco.Router = {
    Registry: {},
    Routes: {},
    Dispacher: {},
    SetDefaults: {
      onLoad: {},
      view: null
    },
    InitController: {},
    Start: {}
  };

  Rojobaco.Data = {
    Registry: {},
    Storage: {},
    initLoad: {},
    Asyn: null
  };

  Rojobaco.Router.Registry = function(path, controller) {
    Rojobaco.Router.Routes[path] = {
      controller: controller
    };
  };

  Rojobaco.Data.Registry = function(path, dataLogic) {
    Rojobaco.Data.Storage[path] = {
      fetch: dataLogic
    };
  };

  Rojobaco.Util.UrlObjectBuilder = function(url) {
    var params, urlObject;
    urlObject = {
      routePath: '',
      routeName: ''
    };
    if (url.indexOf("#") > 0) {
      urlObject.routePath = url.split('#')[1];
      urlObject.routeName = url.split('#')[1];
      if (url.indexOf("#") > 1) {
        params = url.split('#');
        params.shift();
        params.shift();
      } else {
        params = null;
      }
      Rojobaco.Util.Url = {
        Params: params
      };
    }
    if (urlObject.routePath === '') {
      urlObject.routePath = Rojobaco.Router.SetDefaults.view;
      urlObject.routeName = Rojobaco.Router.SetDefaults.view;
    }
    return urlObject;
  };

  Rojobaco.Router.InitController = function(route) {
    if (Rojobaco.Router.Routes[route.routePath]) {
      new Rojobaco.Router.Routes[route.routePath].controller();
    }
  };

  Rojobaco.Data.initLoad = function(route) {
    if (Rojobaco.Data.Storage[route.routePath]) {
      return Rojobaco.Data.Storage[route.routePath];
    } else {
      return null;
    }
  };

  Rojobaco.Router.Start = function() {
    var initDispacher;
    $(window).on('hashchange', function() {
      initDispacher();
    });
    window.addEventListener('load', function() {
      initDispacher();
    });
    initDispacher = function() {
      var urlObject;
      urlObject = Rojobaco.Util.UrlObjectBuilder(location.href);
      Rojobaco.Router.SetDefaults.onLoad(urlObject, Rojobaco.Data.initLoad(urlObject));
      Rojobaco.Router.InitController(urlObject);
    };
    $(window).trigger('hashchange');
  };

}).call(this);
