Rojobaco.Router = {
    Registry:{},
    Routes:{},
    Dispacher:{},
    SetDefaults:{
        onLoad:{},
        view:null
    },
    InitController:{},
    Start: {}
}

Rojobaco.Router.Registry = (path, controller) ->
    Rojobaco.Router.Routes[path] = {controller:controller}
    return

Rojobaco.Util.UrlObjectBuilder = (url) ->
    urlObject = {routePath:'', routeName:''}
    if url.indexOf("#") > 0
        urlObject.routePath = url.split('#')[1]
        urlObject.routeName = url.split('#')[1]

    if urlObject.routePath == ''
        urlObject.routePath = Rojobaco.Router.SetDefaults.view
        urlObject.routeName = Rojobaco.Router.SetDefaults.view
    
    return urlObject

Rojobaco.Router.InitController = (route) ->

    if Rojobaco.Router.Routes[route.routePath]
        new Rojobaco.Router.Routes[route.routePath].controller()
    return

Rojobaco.Router.Start = () ->
    $(window).on 'hashchange', () ->
        initDispacher()
        return
    window.addEventListener 'load', () ->
        initDispacher()
        return
    initDispacher = () ->
        urlObject = Rojobaco.Util.UrlObjectBuilder location.href
        Rojobaco.Router.SetDefaults.onLoad(urlObject)
        Rojobaco.Router.InitController(urlObject)
        return     
    return
$(window).trigger 'hashchange' 