Rojobaco.Util = {}
Rojobaco.Router = {
    Registry:{},
    Routes:{},
    Dispacher:{},
    SetDefaults:{
        onLoad:{},
        view:null
    },
    InitController:{},
    Start: {}
}

Rojobaco.Data = {
    Registry:{},
    Storage:{},
    initLoad:{},
    Asyn: null
}

Rojobaco.Router.Registry = (path, controller) ->
    Rojobaco.Router.Routes[path] = {controller:controller}
    return

Rojobaco.Data.Registry = (path, dataLogic) ->
    Rojobaco.Data.Storage[path] = {fetch:dataLogic}
    return