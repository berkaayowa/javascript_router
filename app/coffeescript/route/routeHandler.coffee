
Rojobaco.Util.UrlObjectBuilder = (url) ->

    urlObject = {routePath:'', routeName:''}
    if url.indexOf("#") > 0
        urlObject.routePath = url.split('#')[1]
        urlObject.routeName = url.split('#')[1]

        if url.indexOf("#") > 1
            params = url.split '#'
            params.shift()
            params.shift()

        else
            params = null
            
        Rojobaco.Util.Url = {Params:params}

    if urlObject.routePath == ''
        urlObject.routePath = Rojobaco.Router.SetDefaults.view
        urlObject.routeName = Rojobaco.Router.SetDefaults.view
    
    return urlObject

Rojobaco.Router.InitController = (route) ->

    if Rojobaco.Router.Routes[route.routePath]
        new Rojobaco.Router.Routes[route.routePath].controller()
    return

Rojobaco.Data.initLoad = (route) ->

    if Rojobaco.Data.Storage[route.routePath]
        return Rojobaco.Data.Storage[route.routePath]
    else
        return null
    return

Rojobaco.Router.Start = () ->

    $(window).on 'hashchange', () ->
        initDispacher()
        return
    window.addEventListener 'load', () ->
        initDispacher()
        return
    initDispacher = () ->
        urlObject = Rojobaco.Util.UrlObjectBuilder location.href       
        Rojobaco.Router.SetDefaults.onLoad urlObject, Rojobaco.Data.initLoad urlObject
        Rojobaco.Router.InitController urlObject
        return 

    $(window).trigger('hashchange')    
    return
