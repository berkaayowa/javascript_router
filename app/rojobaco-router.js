(function() {
  var bindFormInputs, template;

  Rojobaco.Util = {};

  Rojobaco.Router = {
    Registry: {},
    Routes: {},
    Dispacher: {},
    SetDefaults: {
      onLoad: {},
      view: null
    },
    InitController: {},
    Start: {}
  };

  Rojobaco.Data = {
    Registry: {},
    Storage: {},
    initLoad: {}
  };

  Rojobaco.Router.Registry = function(path, controller) {
    Rojobaco.Router.Routes[path] = {
      controller: controller
    };
  };

  Rojobaco.Data.Registry = function(path, load) {
    Rojobaco.Data.Storage[path] = {
      load: load
    };
  };

  template = {
    home: '<h1>welcome home</h1>',
    register: '<h1>in register</h1>',
    signup: '<h1>in sign up</h1>',
    login: '<h1>in login</h1>'
  };

  Rojobaco.Router.Registry('register', function() {
    console.log(" in  regi...");
  });

  Rojobaco.Router.Registry('home', function() {});

  Rojobaco.Router.Registry('login', function() {
    console.log('oooo in  login');
  });

  Rojobaco.Util.UrlObjectBuilder = function(url) {
    var urlObject;
    urlObject = {
      routePath: '',
      routeName: ''
    };
    if (url.indexOf("#") > 0) {
      urlObject.routePath = url.split('#')[1];
      urlObject.routeName = url.split('#')[1];
    }
    if (urlObject.routePath === '') {
      urlObject.routePath = Rojobaco.Router.SetDefaults.view;
      urlObject.routeName = Rojobaco.Router.SetDefaults.view;
    }
    return urlObject;
  };

  Rojobaco.Router.InitController = function(route) {
    if (Rojobaco.Router.Routes[route.routePath]) {
      new Rojobaco.Router.Routes[route.routePath].controller();
    }
  };

  Rojobaco.Data.initLoad = function(route) {
    if (Rojobaco.Data.Storage[route.routePath]) {
      return new Rojobaco.Data.Storage[route.routePath].load();
    } else {
      return null;
    }
  };

  Rojobaco.Router.Start = function() {
    var initDispacher;
    $(window).on('hashchange', function() {
      initDispacher();
    });
    window.addEventListener('load', function() {
      initDispacher();
    });
    initDispacher = function() {
      var urlObject;
      urlObject = Rojobaco.Util.UrlObjectBuilder(location.href);
      Rojobaco.Router.SetDefaults.onLoad(urlObject, Rojobaco.Data.initLoad(urlObject));
      Rojobaco.Router.InitController(urlObject);
    };
    $(window).trigger('hashchange');
  };

  bindFormInputs = function() {
    console.log("loading plugins...");
  };

  $(document).ready(function() {
    Rojobaco.Router.SetDefaults.view = 'home';
    Rojobaco.Router.SetDefaults.onLoad = function(routeOject) {
      console.log(routeOject);
      $('#mainContent').html(template[routeOject.routeName]);
      bindFormInputs();
    };
    Rojobaco.Router.Start();
  });

}).call(this);
